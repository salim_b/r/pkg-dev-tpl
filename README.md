# R Markdown Package Development Template

This is intended as a starting point for the development of R packages using a [literate programming](https://en.wikipedia.org/wiki/Literate_programming)
approach [originally proposed by Yihui Xie](https://yihui.name/rlp/) and re-implemented using the [pkgpurl](https://pkgpurl.rpkg.dev/) package.

## Instructions

1.  Copy all files and folders from the [`tpl/`](tpl) subfolder to the root of your new R package project. You can download it as a ZIP archive from
    [here](https://gitlab.com/salim_b/r/pkg-dev-tpl/-/archive/master/pkg-dev-tpl-master.zip?path=tpl).

2.  Rename the file `gitignore` to `.gitignore`.

3.  Rename all the `RENAME_ME.*` files to match your new R package.

4.  Depending on whether you plan to re-export functions from other packages, either adapt or delete the file `R/reexports.R`.

5.  If you're not going to make use of [pal's canonicalized way to package configuration](https://pal.rpkg.dev/reference/#package-configuration), delete the
    `@section Package configuration` from `R/<PKG>-package.R` as well as the `pkg_config` object from `Rmd/sysdata.nopurl.Rmd`.

6.  Depending on whether you're gonna define ["internal data" in `R/sysdata.rda`](https://r-pkgs.org/data.html#sec-data-sysdata), either adapt or delete the
    whole R Markdown source file `Rmd/sysdata.nopurl.Rmd`.

7.  Depending on whether you're gonna [export data (stored under `data/`) in your package](https://r-pkgs.org/data.html#sec-data-data), either adapt or delete
    the whole R Markdown source file `Rmd/data.nopurl.Rmd`.

8.  Make sure the [R options](https://rdrr.io/r/base/options.html) `usethis.description` and `usethis.full_name` [are
    set](https://usethis.r-lib.org/articles/articles/usethis-setup.html)[^1] and then run
    [`usethis::use_description()`](https://usethis.r-lib.org/reference/use_description.html) to initialize your new package.

9.  Write your package's code in R Markdown files under `Rmd/` and run [`pkgpurl::process_pkg()`](https://pkgpurl.rpkg.dev/reference/process_pkg.html) to
    generate the corresponding R code, document, and build your package. For convenience, it's recommended to assign a keyboard shortcut to
    `pkgpurl::process_pkg()`[^2].

10. Depending on whether you plan to have a static [pkgdown](https://pkgdown.r-lib.org/) website for your package,

    1.  add a new Netlify site pointing to your Git forge (GitHub, GitLab etc.) that serves the `docs/` subfolder of your package's repository.

    2.  either change the `url` and possibly other keys in `pkgdown/_pkgdown.yml` to [match your
        needs](https://pkgdown.r-lib.org/reference/build_site.html#general-config) or delete the `pkgdown` folder and the `netlify.toml` file.

    3.  either adapt the code chunk labelled `documentation` in the `README.Rmd` to the Netlify site created above or delete it.

## Additional hints

- To add R package dependencies in bulk to the `DESCRIPTION`, use [`pal::use_pkg()`]():

  ```r
  c("checkmate",
    "cli",
    "dplyr",
    "glue",
    "fs",
    "httr2",
    "magrittr",
    "pal",
    "purrr",
    "rlang",
    "stringr",
    "tibble") |>
    purrr::walk(\(x) pal::use_pkg(x))
  ```

----------------------------------------------------------------------------------------------------------------------------------------------------------------

[^1]: To see the current values the `DESCRIPTION` will be based on, run
    [`usethis::use_description_defaults()`](https://usethis.r-lib.org/reference/use_description.html).

[^2]: Specifying a keyboard shortcut for `pkgpurl::process_pkg()` comes in very handy. To do so in RStudio, navigate to <kbd>Tools</kbd>→<kbd>Modify Keyboard
    Shortcuts…</kbd> and search for `Process R Markdown package`. Make sure to assign a key combination which isn't [already
    occupied](https://support.rstudio.com/hc/en-us/articles/200711853-Keyboard-Shortcuts). Personally, I use <kbd>Ctrl</kbd>+<kbd>Shift</kbd>+<kbd>V</kbd>.

    This works because the function is [registered as an RStudio add-in](https://rstudio.github.io/rstudioaddins/#registering-addins).
